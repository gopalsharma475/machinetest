<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Employee $employee
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $employee->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $employee->employee_name)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Employees'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="employees form large-9 medium-8 columns content">
    <?= $this->Form->create($employee) ?>
    <fieldset>
        <legend><?= __('Edit Employee') ?></legend>
        <?php
            echo $this->Form->control('employee_name',['name'=>'employee_name','label'=>'Employee Name']);
            echo $this->Form->control('address',['name'=>'address','label'=>'Employee Address']);
            echo $this->Form->control('email',['name'=>'email','label'=>'Email Address']);
            echo $this->Form->control('phone',['name'=>'phone','label'=>'Phone']);
            echo $this->Form->control('date_of_birth',['name'=>'date_of_birth','label'=>'Date of Birth','type'=>'text','id'=>'datepicker','value'=>$this->Time->format($employee->date_of_birth, 'Y/MM/d')]);
            echo $this->Form->control('image',['name'=>'image','type'=>'file','label'=>'Employee Image']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
