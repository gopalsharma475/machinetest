<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Employees Controller
 *
 * @property \App\Model\Table\EmployeesTable $Employees
 *
 * @method \App\Model\Entity\Employee[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class EmployeesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        if ($this->request->query && !empty($this->request->query['employee_name'])) {
            $employeeName = $this->request->query['employee_name'];
            
            $this->paginate = array(
                'conditions' => array(
                    'Employees.employee_name LIKE' => "%$employeeName%"
                )
            );
            $employees = $this->paginate($this->Employees);
        } else {
            $employees = $this->paginate($this->Employees);
        }
        $this->set(compact('employees'));
    }

    /**
     * View method
     *
     * @param string|null $id Employee id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $employee = $this->Employees->get($id, [
            'contain' => [],
        ]);

        $this->set('employee', $employee);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $employee = $this->Employees->newEntity();
        if ($this->request->is('post')) {
            if(!empty($this->request->data['image']['name']))
            {
                $file = $this->request->data['image']; //put the  data into a var for easy use
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('jpg', 'jpeg', 'gif'); //set allowed extensions
                if(in_array($ext, $arr_ext))
                {
                    $imageName =  time().'_'.$file['name'];
                    //echo $imageName; die;
                    if(move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/upload_folder' . DS . $imageName))
                    {
                        $this->request->data['image'] = $imageName;
                        $employee = $this->Employees->patchEntity($employee, $this->request->getData());
                        if ($this->Employees->save($employee)) {
                            $this->Flash->success(__('The employee has been saved.'));

                            return $this->redirect(['action' => 'index']);
                        } else {
                            $this->Flash->error(__('The employee could not be saved. Please, try again.'));
                        }
                    }
                }
            }
            else
            {
                $this->Session->setFlash(__('The data could not be saved. Please, Choose your image.'), 'default',array('class'=>'errors'));
            }
        }
        $this->set(compact('employee'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Employee id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->Employees->validator()->remove('image');
        $employee = $this->Employees->get($id, [
            'contain' => [],
        ]);
        
        if ($this->request->is(['patch', 'post', 'put'])) {

            if(!empty($this->request->data['image']['name']))
            {

                $file = $this->request->data['image']; //put the  data into a var for easy use
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('jpg', 'jpeg', 'gif'); //set allowed extensions
                if(in_array($ext, $arr_ext))
                {
                    $imageName =  time().'_'.$file['name'];
                    //echo $imageName; die;
                    if(move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/upload_folder' . DS . $imageName))
                    {
                        $this->request->data['image'] = $imageName;
                        $employee = $this->Employees->patchEntity($employee, $this->request->getData());
                        if ($this->Employees->save($employee)) {
                            $this->Flash->success(__('The employee has been saved.'));

                            return $this->redirect(['action' => 'index']);
                        } else {
                            $this->Flash->error(__('The employee could not be saved. Please, try again.'));
                        }
                    }
                }

            } else {

                $employee = $this->Employees->patchEntity($employee, $this->request->getData());
                if ($this->Employees->save($employee)) {
                    $this->Flash->success(__('The employee has been saved.'));

                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The employee could not be saved. Please, try again.'));
                }
            }
        }
        $this->set(compact('employee'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Employee id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $employee = $this->Employees->get($id);
        if ($this->Employees->delete($employee)) {
            $this->Flash->success(__('The employee has been deleted.'));
        } else {
            $this->Flash->error(__('The employee could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
