Time Bifurcation

Project Setup :- 20 Mins
Employees Manager :- 10 Mins 
Image Upload Functionality :- 20 Mins
Employee Search By Name :- 10 Mins 
Validation, DatePicker and others :- 20 Min 

Total Time :- 1:20 Mins


Project Setup just follow following steps

1. Opem cmd 
2. Run command on cmd "composer self-update && composer create-project --prefer-dist cakephp/app:^3.8 machinetest" (Before run this command make sure on your system composer should be installed properly.)
Above commend will create and skeleton for your application very quickly. We can do it manually too.
3. Now enter in your appliction using this command "cd machinetest"
Note :- Before using following command make sure you have created tables for your modules. If we will do like this exercise following commend will create lots of Functionality for you like validation and others.
4. Now we will create model using this command "bin/cake bake model employees"
5. Now we will create controller using this command "bin/cake bake controller employees"
6. Now we will create template or view part using this command "bin/cake bake template employees"
"
7. Run command "bin/cake server" to run your application

Database Configuration 

Create an database in your local machine
Go to your application search for config->app_local.php 
open app_local.php file and inside this file please search following code 

'Datasources' => [
        'default' => [
            'host' => 'localhost',
            /*
             * CakePHP will use the default DB port based on the driver selected
             * MySQL on MAMP uses port 8889, MAMP users will want to uncomment
             * the following line and set the port accordingly
             */
            'port' => '3309', // PORT  NAME
            'username' => 'root', // Username of your database
            'password' => '', // Password for your database
            'database' => 'management', // Database name 
            'log' => true,
            'url' => env('DATABASE_URL', null),
        ],
    ],

put all needed information like username, password and your database name. After that you will find that your database will get connected.



